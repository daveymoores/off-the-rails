// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function(){

	if($('#no_age_cookie').length) {
		$('#no_age_cookie').appendTo('#start_modal').show().css('visibility', 'visible');
		$('#start_modal').foundation('reveal', 'open');
		$('.chosen-container').css('width', '31.5%');
	}


	new Share(".share_btn", {
		ui: {
		  flyout: "bottom center" 
		},
	  networks: {
	    facebook: {
	      app_id: "abc123"
	    }
	  }
	});

	new Share(".share_btn", {
		ui: {
		  flyout: "bottom center" 
		},
		networks: {
		    google_plus: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/offtherails/"
		    },
		    twitter: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/offtherails/",
		      description:    "Win Your autumn/winter wardrobe with Off The Rails – Worth £2500!"
		    },
		    facebook: {
		      enabled: true,
		      load_sdk: true,// Load the FB SDK. If false, it will default to Facebook's sharer.php implementation. 
		                // NOTE: This will disable the ability to dynamically set values and rely directly on applicable Open Graph tags.
		                // [Default: true]
		      url: "http://uk-microsites.ign.com/offtherails/",
		      //app_id: "551643344962507",
		      title: "Win Your autumn/winter wardrobe with Off The Rails – Worth £2500!",
		      caption: "Win Your autumn/winter wardrobe with Off The Rails – Worth £2500!",
		      description:    "Win Your autumn/winter wardrobe with Off The Rails – Worth £2500!"
		      //image: // image to be shared to Facebook [Default: config.image]
		    },
		    pinterest: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/offtherails/",
		      //image:   // image to be shared to Pinterest [Default: config.image]
		      description:    "Win Your autumn/winter wardrobe with Off The Rails – Worth £2500!"
		    },
		    email: {
		      enabled: true,
		      title:     "Win Your autumn/winter wardrobe with Off The Rails – Worth £2500!",
		      description:    "Win Your autumn/winter wardrobe with Off The Rails – Worth £2500!"
		    }
		  }
	});


	$('.slider').slick({
	  dots: true,
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  arrows: false,
	  autoplay: true,
	  autoplaySpeed: 2000,
	  cssEase: 'ease',
	  speed: '1000',
	  responsive: [
	    {
	      breakpoint: 600,
	      settings: {
	      	arrows: false,
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	      	arrows: false,
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});
});
