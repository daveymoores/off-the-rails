<?php

/////////////////////////////////////////
// @ Example usage of the HK API
// @ 15th February 2013
// @ Author: Ashley Ford
// @ ashley@harkable.com
////////////////////////////////////////

include('class/HkCompAPI.class.php');

$competition = new HkCompAPI();

    if(isset($_POST['_submit'])){
    
      //$_POST['dob'] = $_POST['_year'].'-'.$_POST['_month'].'-'.$_POST['_day'];
    
      $validation = array(
  
        //@ Format of the array is as follows:
        //@ Input Value Name
        //@ Error Message
        //@ Required, valid_email, over_18, captcha (value must be blank)     
        
      array('fname',    "Please enter your first name",          'required'),
      array('lname',    "Please enter your last name",           'required'),
      //array('dob',      "This promotion is open to users over 18!",  'over_18'),
      array('email',    "Please enter a valid email address",      'valid_email'),
      //array('terms',    "Please Accept the Terms &amp; Conditions",  'required'),
      array('subscribe',  "Please Accept the Terms &amp; Conditions",  'required'),
      array('_captcha', "We're not sure you're a human...",      'captcha')
  
    );  
    
    // validate for errors
    $errors = $competition->validate($validation);
    
    // validate returns FALSE if no errors are found
    if($errors===FALSE){
      // save the data via the api call with the campaign name
          $save = $competition->save('ign_offtherails');
                    
          if(isset($save['_auth']) && $save['_auth']=='failed'){
            echo 'Error Authenticating With the API';           
          }
             
            // if the data was saved successfully   
          if(isset($save['_saved'])==TRUE){
          
          //redirect to thankyou page
            
          header('location: thankyou.php');

          }
                     
        }
                      
    } 
?>


<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Off The Rails in association with AskMen - Win Your autumn/winter wardrobe with Off The Rails worth £2500</title>

    <meta property="og:title" content="Off The Rails - Win Your autumn/winter wardrobe with Off The Rails – Worth £2500">
    <meta property="og:type" content="article">
    <meta property="og:site_name" content="Off The Rails - Win Your autumn/winter wardrobe with Off The Rails – Worth £2500">
    <meta property="og:url" content="uk-microsites.ign.com/offtherails/">
    <meta property="og:description" content="Win Your autumn/winter wardrobe with Off The Rails – Worth £2500">
    <meta property="og:image" content="img/rails_logo.png">
    <meta property="fb:admins" content="546507370">

    <link rel="shortcut icon" href="http://uk-microsites.ign.com/ciroc/favicon.ico">
    <link rel=apple-touch-icon href="apple-touch-icon.png">
    <link rel=apple-touch-icon sizes=72x72 href="apple-touch-icon-72x72.png">
    <link rel=apple-touch-icon sizes=114x114 href="apple-touch-icon-114x114.png">

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.3.7/slick.css"/>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
     <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>


     <script src="//use.typekit.net/cbs6ikw.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>

    <script src="js/hide_it.js"></script>

    
  </head>
  
  
  
<body>

 <script src="js/output_age_form.js"></script>

<!--uncomment for live-->
<div id="policyNotice" style="display:none">
    <div class="close-btn">
        <a href="#_" title="Close" data-domain=".ign.com">Close</a>
    </div>
    <p>We use cookies and other technologies to improve your online experience. By using this Site, you consent to this use as described in our <a href="http://corp.ign.com/policies/cookie-eu" target="_blank">Cookie Policy</a>.</p>
</div>
<!--uncomment for live-->

    <!--off canvas menu-->
  <div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
      <nav class="tab-bar top" data-topbar role="navigation">

          <section class="left-small show-for-small-only">
              <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
          </section>

          <div class="row">

            <div class="ign_red-wrapper">
              <h1 class="title"><img src="img/am-logo.png" alt="AskMen logo"></h1>
            </div>

            <ul class="left share_btn" id="share_override">
              <li><a href="#"></a></li>
            </ul>

          </div>

      </nav>

      <aside class="left-off-canvas-menu show-for-small-only">
        <ul class="off-canvas-list">
          <li><label>Off The Rails</label></li>
          <li><a href="#">offtherails.com</a></li>
        </ul>
      </aside>
      <!--end off canvas menu-->


        <section class="main-section">
          <!-- content goes here -->

          <div class="row head">
            <div class="large-4 medium-5 small-8 large-centered medium-centered small-centered columns">
              <img src="img/rails_logo.png" alt="off the rails">
            </div>
          </div>

          <div class="row comp_body">
            <div class="large-10 medium-10 small-11 large-centered medium-centered small-centered columns wrapper">

              <h1><b>Win</b> Your autumn/winter wardrobe<br> with Off The Rails – <b>Worth £2500!</b></h1>

              <div class="slider">
                <div><img src="img/carousel/Leo-Joseph-Clifton-Swim-Shorts.jpg" alt=""></div>
                <div><img src="img/carousel/London-sock-co.-East-India-Saffron.jpg" alt=""></div>
                <div><img src="img/carousel/Mark-Thomas-Taylor.jpg" alt=""></div>
                <div><img src="img/carousel/2014_01_08-Notch-AW14-11-077v1.jpg" alt=""></div>
                <div><img src="img/carousel/Brutus-Jamaican-Shirt.jpg" alt=""></div>
                <div><img src="img/carousel/Backpack-Black-Front.jpg" alt=""></div>
                <div><img src="img/carousel/Dixon_CedarGrain.jpg" alt=""></div>
                <div><img src="img/carousel/Harry-Stedman-Jumper.jpg" alt=""></div>
                <div><img src="img/carousel/Marwood-Cherchbi-Travel-Collaboration_6.jpg" alt=""></div>
                <div><img src="img/carousel/Nicce-manor-sweat.jpg" alt=""></div>
                <div><img src="img/carousel/oppermann-vallance-black-2.jpg" alt=""></div>
                <div><img src="img/carousel/Pocket-Chief-mint.jpg" alt=""></div>
                <div><img src="img/carousel/Red-Paisley-Two-Direction.jpg" alt=""></div>
                <div><img src="img/carousel/RH_NEW_FOUR_EYES_72.jpg" alt=""></div>
                <div><img src="img/carousel/shirt_IMG_0735_memphis.jpg" alt=""></div>
              </div>

              <div class="row">
                <div class="large-11 large-centered columns">
                  <p>This October 30th – November 2nd, AskMen has teamed up with brand new menswear event OFF THE RAILS, which is being held at The Truman Brewery, London.
Visitors to Off The Rails can shop from over 70 menswear brands, receive a complimentary personal styling session, browse the Future Art 10 gallery, eat an award winning Patty &amp; Bun burger and even book in for a haircut with the on-site Murdock London Barbershop. And that’s not to mention the DJs and bars which will be popping up for the weekend.</p>
                  <p>To celebrate, we’re giving away an awesome package of products from the designers who will be repping their Autumn/Winter lines at the event. From jeans to shirts to knits via luxe socks,<br> we’ve got you covered.</p>
                  <p>Tickets for Off The Rails start at just £6 so if you want to book tickets <br>head over to <a href="http://offtherails.com">offtherails.com</a> now to book</p>
                </div>
              </div>

               <div class="row">
                  <div class="large-8 large-centered columns">
                      
                      <?php 

                              if(isset($errors)){
                                  // if there were errors in form processing show them here
                                  echo '<div class="alert alert-error"><ul>';                  
                                      foreach($errors as $error){           
                                        echo '<li class="alert-box alert round">'.$error.'</li>';  
                                      }         
                                  echo '</ul></div>';
                              
                              }
                      ?>

              <form method="post" class="form-horizontal">
                  <h2>Enter Your Details</h2>
                  <div class="row">
                    <div class="large-6 columns">
                      <label>FIRST NAME
                       
                        <input type="text" name="fname" value="<?php if(isset($_POST['fname'])){echo $_POST['fname'];}?>"> 
                      </label>
                    </div>
                    <div class="large-6 columns">
                      <label>LAST NAME
                        <input type="text" name="lname" value="<?php if(isset($_POST['lname'])){echo $_POST['lname'];}?>">
                      </label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-12 columns">
                      <label>EMAIL
                        <input type="text" name="email" value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>"> 
                      </label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-12 columns">
                      <div class="row pushDown">
 <!--                        <div class="large-1 column">
                          <input id="checkbox1" type="checkbox" name="terms" value="1">
                        </div>
                        <div class="large-11 column">
                          <label class="checkboxLabel" for="checkbox1">I confirm that I am a UK resident over the age of 18 and have read and understood the <a href="#_">terms and conditions and the privacy policy.</a></label>
                        </div> -->
                      </div>

                        <div class="row">
                          <div class="large-1 medium-1 small-1 column">
                            <input id="checkbox2" type="checkbox" name="subscribe" value="1">
                          </div>
                          <div class="large-11 medium-11 small-11 column">
                            <label class="checkboxLabel" for="checkbox2">I confirm that I am a UK resident over the age of 18 and have read and understood the terms and conditions and the privacy policy. By entering this competition I consent to Off The Rails contacting me with details of runners-up tickets to gain entry into the event.</label>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="large-6 large-centered columns">
                        <input type="hidden" name="_captcha" value=""> 

                        <div class="button_wrapper">
                          <input type="submit" value="ENTER" class="button" name="_submit">
                        </div>

                      </div>
                    </div>
                  <!-- </div> -->
                </form>
                  </div> <!-- container -->
              </div>
            </div>
          </div>

        </section>

        <footer class="tab-bar">
          <div class="row">

            <div class="ign_red-wrapper">
              <div class="title"><img src="img/ign_ent.png" alt="IGN Entertainment logo"></div>

              <ul>
                <li><a href="http://corp.ign.com/privacy.html" title="Privacy Policy">Privacy policy</a></li>
                <li><a href="http://corp.ign.com/user-agreement.html" title="User Agreement">User agreement</a></li>
              </ul>
            </div>

            <ul class="left links">
              <ul>
                <li><p>Copyright 2014<br>IGN Entertainment UK, Inc.</p></li>
                <li><a href="http://uk.corp.ign.com/#about" title="About Us">About Us</a></li>
                <li><a href="http://uk.corp.ign.com/#contact" title="Contact Us">Contact Us</a></li>
                <li><a href="http://corp.ign.com/feeds.html" title="RSS Feeds">RSS Feeds</a></li>
              </ul>
            </ul>

          </div>
        </footer>

      <a class="exit-off-canvas"></a>

    </div>
  </div>


<div id="start_modal" class="reveal-modal small">
    <p>Please confirm you are 18 years of age or over, if you are not please <a href="http://askmen.com">go back to AskMen</a></p>
</div>


    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/share.min.js"></script>
    <script src="js/ageChecker.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.7/slick.min.js"></script>


    <script>

      (function(){
          var cookieName = 'persist-policy';
          var cookieValue = 'eu';
          var createCookie = function(name,value,days,domain) {
              var expires = '';
              var verifiedDomain = '';
              if (days) {
                  var date = new Date();
                  date.setTime(date.getTime()+(days*24*60*60*1000));
                  expires = '; expires='+date.toGMTString();
              }
              if (domain) {
                  verifiedDomain = '; domain='+domain;
              }
              document.cookie = name+'='+value+expires+verifiedDomain+'; path=/';
          };
          var readCookie = function(name) {
              var nameEQ = name + "=";
              var ca = document.cookie.split(';');
              for(var i=0;i < ca.length;i++) {
                  var c = ca[i];
                  while (c.charAt(0)==' ') c = c.substring(1,c.length);
                  if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
              }
              return null;
          };

          window.initPolicyWidget = function(){
              jQuery('#policyNotice').show().find('.close-btn a').click(function(e){
                  createCookie(cookieName, cookieValue, 180, jQuery(this).data('domain'));
                  jQuery('#policyNotice').remove();
                  return false;
              });
          }
          var cookieContent = readCookie(cookieName);
          if (typeof  cookieContent === 'undefined' || cookieContent != cookieValue) {
              jQuery(document).ready(initPolicyWidget);
              jQuery(document).trigger('policyWidgetReady');
          }
      })();

  </script>


  <!--begin GA script-->
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-15279170-1']);
    _gaq.push(['_trackPageview', 'CAMPAIGN_NAME']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>
  <!--end GA script-->

  <!-- Begin comScore Tag -->
  <script>
    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "3000068" });
    (function() {
      var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
      s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
      el.parentNode.insertBefore(s, el);
    })();
  </script>
  <noscript>
    <img src="http://b.scorecardresearch.com/p?c1=2&c2=3000068&cv=2.0&cj=1" />
  </noscript>
   <!-- End comScore Tag -->
   <!--
  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
-->
  </body>
</html>
